#!/usr/bin/env python
# coding=utf-8

import sys, os, os.path, subprocess, re, json, md5, shutil
import buildlib as bl

destination = 'src/main/resources/irias/app/www'
frontend_destination = 'target/classes/irias/app/www'
srcdir = os.path.split(os.path.dirname(sys.argv[0]))[1] + '/'  # figure out the path-relative directory name where this script resides



def usage():
	print >>sys.stderr, 'usage: clean-frontend.py clean'
	sys.exit(1)

def main(prog, *args):
	args = list(args)
	if args == []:
		args = ['install']
	for t in args:
		if len(args) > 1:
			print >>sys.stderr, '# %s:' % t

		if t == 'clean':
			bl.remove_tree(frontend_destination)

		else:
			usage()

if __name__ == '__main__':
	main(*sys.argv)
