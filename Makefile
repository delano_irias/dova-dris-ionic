run: build
	mvn -B -Drevision=dev -Djava.util.logging.config.file="config/logging.properties" -Dexec.args="serve local/config.json" exec:java

build:
	mvn -B -Drevision=dev compile

sql:
	python sql-upgrades.py sql

clean:
	-mvn -B -Drevision=`git describe --tags | sed 's/^v//'` clean

frontend:
	cd www/ && ionic build --prod

watch:
	cd www/ && ionic build --watch

debug:
	cd www/ && ionic build --prod --aot

component:
	cd www/src/app/components/ && ng g component

service:
	cd www/src/app/services/ && ng g service

directive:
	cd www/src/app/directives/ && ng g directive

test:
	mvn -B -Drevision=dev -Dtest_config="local/config-test.json" test

jar:
	mvn -B -Dtest_config="local/config-test.json" -Drevision=`git describe --tags | sed 's/^v//'` clean install

release:
	cd www/ && ionic build --prod
	mvn -B -DskipTests -Dtest_config="local/config-test.json" install
	echo release: drisscanner.jar target/drisscanner-build.jar

production:
	cd www/ && ./node_modules/@angular/cli/bin/ng build --prod

report:
	mvn -B -Drevision=dev site

setup:
	mkdir -p www/node_modules/.bin
	cd www/ && npm install

npm:
	cd www/ && npm install

target:
	-rm -rf target/
