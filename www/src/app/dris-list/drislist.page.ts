import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import _ from 'lodash';
import {auth, customer, sherpa} from '../app.module';
import {SherpaError} from '../classes/sherpa/SherpaError';
import jsQR from 'jsqr';
import {LoadingController, Platform, ToastController} from '@ionic/angular';
import {ActivatedRoute, ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {Dris} from "../classes/Dris";
import {MapService} from "../services/map.service";

@Component({
	selector: 'app-drislist',
	templateUrl: 'drislist.page.html',
	styleUrls: ['drislist.page.scss']
})
export class DrisListPage{


	public drisses: Dris[] = [];

	constructor(
		private toastCtrl: ToastController,
		private loadingCtrl: LoadingController,
		private plt: Platform,
		private route: ActivatedRoute,
		private router: Router
	) {

		this.route.queryParams.subscribe(params => {
			if (params.drisses) {
				this.drisses = JSON.parse(params.drisses);
			}
		});
	}

	clickDris(dris: Dris){
		this.router.navigate(['tabs/tab1'], {queryParams: {dris_id: dris.dris_id, qrcode: dris.qr_code}});
	}


}
