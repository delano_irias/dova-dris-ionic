import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteDetailComponent } from '../route/route-detail/route-detail.component';
import {AuthService} from '../services/auth.service';
import {DrisListPage} from "./drislist.page";

const routes: Routes = [
  {
    path: '',
    component: DrisListPage,
    canActivate: [AuthService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DrislistRoutingModule { }
