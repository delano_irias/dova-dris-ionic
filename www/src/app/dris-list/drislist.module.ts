import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {DrisListPage} from './drislist.page';

import { RouteDetailComponentModule } from '../route/route-detail/item.module';
import {DrislistRoutingModule} from "./drislist-routing.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouteDetailComponentModule,
	DrislistRoutingModule
  ],
  declarations: [DrisListPage]

})
export class DrislistModule { }
