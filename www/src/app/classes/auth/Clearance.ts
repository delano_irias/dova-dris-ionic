import {AuthParameter} from "./User";
import _ from 'lodash';

export class Clearance {

	readonly division_id: number = 0;
	division_name: string;
	division_code: string;
	role_code: RoleCode;
	params: AuthParameter[];

	constructor(name: string, code: RoleCode) {
		this.division_name = name;
		this.role_code = code;
	}
}

export enum RoleCode {
	ADMIN = 'admin',
	USER = 'user'
}

export enum ClearanceType {
	VERVOERDER = 'vervoerder',
	HALTEDATABEHEERDER = 'haltedatabeheerder',
	OV_AUTORITEIT = 'ov-autoriteit',
	DOVA_ADMIN = 'dova-admin'
}