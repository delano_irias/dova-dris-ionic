import {Clearance, ClearanceType, RoleCode} from "./Clearance";
import _ from 'lodash';

export class AuthParameter {
	key : string;
	value : string;
}

export class User {

	readonly user_id: number;
	username: string;
	name: string;
	email: string;
	is_root: boolean;
	is_active: boolean;
	phone: string;
	address: string;
	notes: string;
	clearance: Clearance[];
	public params: AuthParameter[];


	roles?: ClearanceType[] = [];
	is_temporary_password?: boolean = false;
	priv_stat_accepted?: boolean = false;

	constructor() {
	}


	getRoles(): ClearanceType[] {
		let roles = new Set<ClearanceType>();

		_.forEach(this.clearance, (c: Clearance) => {
			if (c.division_code === 'dova-br-hb-vervoerder') {
				if(c.role_code === RoleCode.USER) {
					roles.add(ClearanceType.VERVOERDER);
				}
			}
			else if (_.startsWith(c.division_code, 'dova-br-hb-')) {
				if (c.role_code === RoleCode.ADMIN) {
					roles.add(ClearanceType.OV_AUTORITEIT);
				}
				else if (c.role_code === RoleCode.USER) {
					roles.add(ClearanceType.HALTEDATABEHEERDER);
				}
			}
			else if (c.division_code === 'dova' && c.role_code === RoleCode.ADMIN) {
				roles.add(ClearanceType.DOVA_ADMIN);
			}
		});

		// @ts-ignore
		return _.toArray(roles);
	}

}
