export interface SherpaJson {
	id: string;
	title: string;
	functions: string[];
	baseurl: string;
	version: string;
	sherpaVersion: number;
}
