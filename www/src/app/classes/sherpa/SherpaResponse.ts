import {SherpaError} from "./SherpaError";
import {SherpaObject} from "./SherpaObject";

export interface SherpaResponse {
	error: SherpaError;
	result: any;
}
