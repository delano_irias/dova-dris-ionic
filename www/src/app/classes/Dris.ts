import {Coordinate} from "ol/coordinate";

export class Dris {
	dris_id: number;
	display_id: string;
	displaynaam: string;
	leverancierscode: string;
	display_leverancierscode: string;
	objectcode_leverancier: string;
	reizigers_identificatie: string;
	beheerverantwoordelijke: string;
	opdrachtgever: string;
	display_functie: string;
	display_type: string;
	display_techniek: string;
	energiebron: string;
	audio: boolean;
	dris_omgeving: string;
	status: string;
	locatie: Coordinate;
	opmerkingen: string;
	karakters_vrije_tekst: number;
	scrollend_alternerend: string;
	qr_code: string;
	dris_koppeling: DrisKoppeling[];
}

export class DrisKoppeling{
	dris_koppeling_id: number;
	dris_id: number;
	quay: string;
	stopplace: string;
}