import {Coordinate} from "ol/coordinate";

export class QuaySimple {
	public geom: Coordinate;
	public quaycode: string;
	public compassdirection: number;
	public quaystatus: string;
	public transportmode: string;
	public stopsidecode: string;
	public quayname: string;
	public town: string;
}

export class Quay extends QuaySimple {
	public town: string;
	public street: string;
	public concessionprovidercode : string;
	public municipalitycode: string;
	public quaytype;
	public quayname: string;
	public last_mutated_at: Date;
}

export class QuayQrcode{
	public quaycode: string;
	public qrcode: string;
}
