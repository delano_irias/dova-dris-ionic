export enum TileServer {
	IRIAS_MAIN = "https://tilecache.irias.nl/tiles/",
	ROUTEMAKER = "https://tilecache-routemaker.irias.nl/tiles/",
	LOCAL = "http://localhost:8040/tiles/"
}