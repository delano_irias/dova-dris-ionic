import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy, ToastController} from '@ionic/angular';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ErrorMessageStrategy, SherpaService} from './services/sherpa.service';
import {HttpClientModule} from '@angular/common/http';
import {SherpaError} from './classes/sherpa/SherpaError';
import {AuthService} from './services/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login/login.component';
import {PasswordComponent} from './login/password/password.component';
import {PasswordResetComponent} from './login/password-reset/password-reset.component';

export let sherpa: SherpaService;
export let auth: AuthService;
export let customer: any;

@NgModule({
	declarations: [AppComponent, LoginComponent, PasswordComponent, PasswordResetComponent],
	entryComponents: [],
	imports: [
		CommonModule,
		BrowserModule,
		IonicModule.forRoot(),
		AppRoutingModule,
		HttpClientModule,
		FormsModule, ReactiveFormsModule
	],
	providers: [{provide: RouteReuseStrategy, useClass: IonicRouteStrategy}, SherpaService, AuthService],
	bootstrap: [AppComponent],
})
export class AppModule {

	constructor(private injector: Injector) {
		sherpa = injector.get(SherpaService);
		auth = injector.get(AuthService);

		this.setup()
			.then(x => {
			})
			.catch(e => {
				console.log('sherpa-docs not initialized', e);
			});
	}

	async setup() {
		const toast = new IonicToastErrorMessage(this.injector.get(ToastController));
		await sherpa.buildSherpa(toast);

		sherpa.api.status();
	}

}

export class IonicToastErrorMessage implements ErrorMessageStrategy {

	constructor(private toastController: ToastController) {
	}

	async show(sherpaError: SherpaError) {
		const toast = await this.toastController.create({
			message: sherpaError.message,
			color: 'danger',
			position: 'middle',
			buttons: [
				{
					text: 'Sluit',
					role: 'cancel',
					handler: () => {}
				}
			]
		});
		await toast.present();
	}

}
