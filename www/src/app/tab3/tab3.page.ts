import { Component } from '@angular/core';
import {auth} from '../app.module';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  constructor() {}

  logout(){
    auth.logout();
  }
}
