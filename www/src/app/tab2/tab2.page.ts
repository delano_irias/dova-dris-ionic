import {AfterViewInit, Component, OnInit} from '@angular/core';
import {auth, sherpa} from '../app.module';
import {Feature, Map as OLMap, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import {OSM} from 'ol/source';
import {MapService} from '../services/map.service';
import _ from 'lodash';
import {ActionSheetController, NavController} from '@ionic/angular';
import {StorageService} from '../services/storage.service';
import { Storage } from '@ionic/storage-angular';
import Geolocation from 'ol/Geolocation';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import {QuayQrcode, QuaySimple} from '../classes/QuaySimple';
import {Point} from 'ol/geom';
import {SherpaError} from '../classes/sherpa/SherpaError';
import {StyleService} from '../services/styles.service';
import {Select} from 'ol/interaction';
import {Router} from '@angular/router';
import {Dris} from "../classes/Dris";

@Component({
	selector: 'app-tab2',
	templateUrl: 'tab2.page.html',
	styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements AfterViewInit {


	drisses: Dris[] = [];
	map: OLMap;
	selectInteraction: Select;
	drisLayer: VectorLayer;
	drisSource: VectorSource;

	maxZoom = 19;
	zoomBoundary = 14;
	curZoom = 1;
	timer_id = -1;

	constructor(private mapService: MapService, private router: Router) {
		this.drisses = [];

		this.drisLayer = this.mapService.getLayer(StyleService.drisIconStyle, 15);
		this.drisLayer.setVisible(true);
		this.drisSource = this.drisLayer.getSource();
		this.drisSource.changed();
	}

	ngAfterViewInit(): void{
		this.setMap();
	}

	setMap() {
		_.delay(() => {
			this.map = this.mapService.createMap('main-map');
			this.map.addLayer(this.drisLayer);
			this.map.getView().setMinZoom(8);

			this.map.on('moveend', (event) => this.onMoveEnd(event));
			this.map.getView().setMaxZoom(this.maxZoom);

			this.startSelection();
		}, 100);


	}

	onMoveEnd(event) {
		let zoom = event.map.getView().getZoom();
		this.curZoom = zoom;

		if (zoom < this.zoomBoundary) {
			this.drisLayer.setVisible(false);
			this.drisSource.clear();
		}
		else{
			this.drisLayer.setVisible(true);

			window.clearTimeout(this.timer_id);
			this.timer_id = _.delay(() => this.fetchByExtent(), 500);
		}

	}

	startSelection() {
		this.selectInteraction = new Select({
			layers: [this.drisLayer],
			multi: true
		});
		this.selectInteraction.on('select', (event) => this.selectHandler(event));
		this.map.addInteraction(this.selectInteraction);
	};

	selectHandler(event) {
		if (event.selected && event.selected.length) {
			let feat = event.selected[0];

			let selectedDris = [];
			_.forEach(event.selected, (item) => {
					let tempFeature = _.clone(item);
					selectedDris.push(tempFeature);
			});

			if(selectedDris.length > 1){
				let passDrisses: Dris[] = [];
				_.forEach(selectedDris, (item) => {
					let tempDris = new Dris();
					tempDris.dris_id = item.get('code');
					tempDris.display_id = item.get('displayid');
					tempDris.displaynaam = item.get('displaynaam');
					tempDris.opmerkingen = item.get('opmerkingen');
					tempDris.qr_code = item.get('qrcode');
					passDrisses.push(tempDris);
				});
				this.router.navigate(['tabs/drislist'], {queryParams: {drisses: JSON.stringify(passDrisses)}});
			}else {
				var code = feat.get('code');
				var qrcode = feat.get('qrcode');
				this.router.navigate(['tabs/tab1'], {queryParams: {dris_id: code, qrcode: qrcode}});
			}

			this.selectInteraction.getFeatures().clear();
		}
	}

	goToLocation(){
		let geolocation = new Geolocation({
			// enableHighAccuracy must be set to true to have the heading value.
			trackingOptions: {
				enableHighAccuracy: true
			},
			projection: this.map.getView().getProjection(),
			tracking: true,
		});

		let mapView = this.map.getView();
		geolocation.on('change:position', function() {
			let coordinates = geolocation.getPosition();
			mapView.setCenter(coordinates);
			mapView.setZoom(15);
			geolocation.setTracking(false);
		});
	}

	fetchByExtent() {
		let extent = this.map.getView().calculateExtent(this.map.getSize());

		sherpa.api.getDRISSESByExtent(auth.token(), extent)
			.then((result: any) => {
				this.drisses = result;
				this.addQuayFeatures(this.drisses);
			})
			.catch((e: SherpaError) => {});
	}

	addQuayFeatures(quays) {
		let features = [];
		this.drisSource.clear();

		_.forEach(quays, (d : Dris) => {
			let feat = new Feature(new Point(d.locatie));
			feat.setId(d.dris_id);
			feat.setProperties({
				'code': d.dris_id,
				'displayid' : d.display_id,
				'displaynaam': d.displaynaam,
				'opmerkingen': d.opmerkingen,
				'leverancierscode': d.leverancierscode,
				'qrcode': d.qr_code
			});

			features.push(feat);
		});

		this.drisSource.addFeatures(features);
		this.drisSource.changed();
	}


	}
