import {Component, OnInit} from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Drivers } from '@ionic/storage';
import {sherpa} from '../app.module';


@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.page.html',
	styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

	constructor(private storage: Storage) {

	}

	ngOnInit() {
	}

}
