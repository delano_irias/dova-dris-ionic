import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import {IonicStorageModule} from '@ionic/storage-angular';
import {Drivers} from '@ionic/storage';

@NgModule({
  imports: [
	CommonModule,
	FormsModule,
	IonicModule,
	DashboardPageRoutingModule,
	IonicStorageModule.forRoot({
	  name: '__rmdb',
	  driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
	}),
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
