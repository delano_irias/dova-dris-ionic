import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteDetailComponent } from './route-detail.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule],
  declarations: [RouteDetailComponent],
  exports: [RouteDetailComponent]
})
export class RouteDetailComponentModule { }
