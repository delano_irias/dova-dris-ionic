import {Injectable} from '@angular/core';
import {Circle as CircleStyle, Fill, Stroke, Style, Text} from 'ol/style';
import Icon from "ol/style/Icon";
import {StyleLike} from "ol/style/Style";

@Injectable({
	providedIn: 'root'
})
export class StyleService {
	private static clusterCache: any = {};


	constructor() {
	}

	static toRadian = function(degrees) {
		return degrees * Math.PI / 180;
	};

	static pointStyle(feature: any, resolution: any) {

		let pointStyle = new Style({
			image: new CircleStyle({
				radius: 6,
				fill: new Fill({
					color: '#9f4c97',
				}),
				stroke: new Stroke({
					color: '#fff',
					width: 1
				}),
			})
		});

		return [pointStyle];
	}


	static drisIconStyle(feature: any, resolution: any): Style[] {
		let available = feature.get('qrcode') === null || feature.get('qrcode')  === undefined;

		let style = new Style({
			image: new CircleStyle({
				radius: 8,
				stroke: new Stroke({
					color: available ? "#7ac74f" : "#a1b0a0",
					width: 5
				}),
				fill: new Fill({
					color: available ? "#00b159" : '#636d62',
				})
			})
		});

		return [style];
	}

	static iconStyle2(feature: any, resolution: any) {
		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		let style = new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot.png' : '/assets/img/icon-dot-gray.png',
				opacity: 0.8,
				scale: 0.9
			})
		});
		return [style];
	}

	static iconStyle3(feature: any, resolution: any) {
		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		let style = new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot.png' : '/assets/img/icon-dot-gray.png',
				opacity: 0.6,
				scale: 0.7
			})
		});
		return [style];
	}

	static directionStylePrimary(feature: any, resolution: any): Style[] {
		let compass = feature.get('compass');
		let radian = StyleService.toRadian(compass - 45); // icon starts at 45 degrees

		let compassStyle = new Style({
			image: new Icon({
				src: '/assets/img/icon-location-arrow2.png',
				rotation: radian,
				size: [54,54],
				rotateWithView: true,
				scale: 1.0
			})
		});

		let style: Style[] = StyleService.iconStyleTransPrimary(feature, resolution);
		style.push(compassStyle);
		return style;
	}


	static directionStyleSecundary(feature: any, resolution: any) {
		let compass = feature.get('compass');
		let radian = StyleService.toRadian(compass - 45); // icon starts at 45 degrees

		let compassStyle = new Style({
			image: new Icon({
				src: '/assets/img/icon-location-arrow2.png',
				rotation: radian,
				size: [54,54],
				rotateWithView: true,
				scale: 0.5
			})
		});

		let style = StyleService.iconStyleTransSecundary(feature, resolution);
		style.push(compassStyle);
		return style;
	}

	static directionStyleTertiary(feature: any, resolution: any) {
		let compass = feature.get('compass');
		let radian = StyleService.toRadian(compass - 45); // icon starts at 45 degrees

		let compassStyle = new Style({
			image: new Icon({
				src: '/assets/img/icon-location-arrow2.png',
				rotation: radian,
				size: [54,54],
				rotateWithView: true,
				scale: 0.75
			})
		});

		let style = StyleService.iconStyleTransTertiary(feature, resolution);
		style.push(compassStyle);
		return style;
	}

	static selectIconStyle(feature: any, resolution: any): Style {

		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		return new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot.png' : '/assets/img/icon-dot-gray.png',
				color: '#9f4c97'
			}),
			zIndex: 100
		});
	}

	static summaryStyle(feature: any, resolution: any): Style[] {
		let styles = [];

		let point = new Style({
			zIndex: feature.get('zIndex'),
			image: new CircleStyle({
				radius: 18,
				fill: new Fill({
					color: 'rgb(159, 76, 151, 1)',
				}),
				stroke: new Stroke({
					color: 'rgb(159, 76, 151, .3)',
					width: 18
				}),
			})
		});

		let border = new Style({
			zIndex: feature.get('zIndex'),
			image: new CircleStyle({
				radius: 18,
				fill: new Fill({
					color: 'rgb(159, 76, 151, 0)',
				}),
				stroke: new Stroke({
					color: '#f9f9f9',
					width: 2
				}),
			})
		});

		let key = feature.get('filter');
		let label = new Style({
			zIndex: feature.get('zIndex'),
			text: new Text({
				font: 'bold 11px sans-serif',
				text: '' + feature.get('quays')[key], // default
				fill: new Fill({
					color: '#fff'
				}),
				padding: [1,1,1,1],
				offsetX: 0,
				offsetY: 0
			})
		});


		styles.push(point);
		styles.push(border);
		styles.push(label);
		return styles;
	}


	static clusterStyleWithDirection(feature: any, resolution: any): Style[] {
		let size = feature.get('features').length;
		if(size === 1) {
			let originalFeature = feature.get('features')[0];
			return StyleService.directionStylePrimary(originalFeature, resolution);
		}

		let style = StyleService.clusterCache['clusterStyle'];

		if(!style) {
			style = new Style({
				image: new CircleStyle({
					radius: 20,
					stroke: new Stroke({
						color: '#fff'
					}),
					fill: new Fill({
						color: 'rgb(159, 76, 151, 1)',
					})
				}),
				text: new Text({
					font: 'bold 12px sans-serif',
					fill: new Fill({
						color: '#fff'
					})
				})
			});
			StyleService.clusterCache['clusterStyle'] = style;
		}

		style.getText().setText(size.toString());
		return [style];
	}


	static pointNewStyle(feature: any, resolution: any) {
		return new Style({
			image: new CircleStyle({
				radius: 12,
				stroke: new Stroke({
					color: 'rgb(159, 76, 151, 1)'
				}),
				fill: new Fill({
					color: 'rgb(159, 76, 151, 0.2)',
				})
			}),
			text: new Text({
				text: '+',
				fill: new Fill({
					color: '#0a0a0a'
				})
			})
		});
	}

	static pointNewStyleArr(feature, resolution){
		let style = this.pointNewStyle(feature, resolution);
		return [style];
	}

	static polygonStyle(feature: any, resolution: any) {

		let fillColor = 'rgba(203,211,255,0.2)';
		let isSelected = feature.get('isSelected');
		if(isSelected){
			fillColor = 'rgba(2, 99, 255, 0.4)';
		}
		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(2, 99, 255, 0.8)',
				lineDash: [5, 5],
				width: 2
			}),
			fill: new Fill({
				color: fillColor,
			})
		});
		return [style];
	}

	static polygonStyle2(feature: any, resolution: any) {

		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(40,40,40,0.4)',
				width: 1.2
			})
		});
		return [style];
	}

	static polygonStyle3(feature: any, resolution: any) {
		let fillColor = 'rgba(203,211,255,0.2)';
		let isSelected = feature.get('isSelected');
		if(isSelected){
			fillColor = 'rgba(2,255,225,0.8)';
		}
		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(2,255,225,0.8)',
				lineDash: [5, 5],
				width: 2
			}),
			fill: new Fill({
				color: fillColor,
			})
		});
		return [style];
	}

	static polygonStyle4(feature: any, resolution: any) {
		let fillColor = 'rgba(213,240,246,0.1)';
		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(159, 76, 151,0.8)',
				lineDash: [5, 5],
				width: 5
			}),
			fill: new Fill({
				color: fillColor,
			})
		});

		let styles = [style];
		return styles;
	}

	static polygonStyle4Mask(feature: any, resolution: any) {
		let fillColor = 'rgba(159, 76, 151,0.3)';
		let style = new Style({
			fill: new Fill({
				color: fillColor,
			})
		});

		let styles = [style];
		return styles;
	}

	static polygonSelectedStyle(feature: any, resolution: any) {

		let fillColor = 'rgba(2, 99, 255, 0.4)';
		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(2, 99, 255, 0.8)',
				lineDash: [5, 5],
				width: 2
			}),
			fill: new Fill({
				color: fillColor,
			})
		});
		return [style];
	}

	static lineStyle(feature: any, resolution: any) {
		let style = new Style({
			stroke: new Stroke({
				color: 'rgba(159,76,151,0.8)',
				lineDash: [4, 4],
				width: 3
			})
		});
		return [style];
	}

	static nwbStyle(feature: any, resolution: any) {
		let style = new Style({
			stroke: new Stroke({
				color: 'rgb(99,193,197)',
				width: 5
			})
		});
		return [style];
	}

	private static iconStyleTransPrimary(feature: any, resolution: any) {
		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		let style = new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot-trans.png' : '/assets/img/icon-dot-gray.png',
			})
		});
		return [style];
	}

	private static iconStyleTransSecundary(feature: any, resolution: any): Style[] {
		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		let style = new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot-trans.png' : '/assets/img/icon-dot-gray.png',
				scale: 0.75
			})
		});
		return [style];
	}

	private static iconStyleTransTertiary(feature: any, resolution: any) {
		let available = feature.get('quaystatus') === 'available' || feature.get('quaystatus')  === 'plan';

		let style = new Style({
			image: new Icon({
				src: available ? '/assets/img/icon-dot-trans.png' : '/assets/img/icon-dot-gray.png',
				scale: 0.5
			})
		});
		return [style];
	}

	static iconStyleInactive(feature: any, resolution: any) {
		let style = new Style({
			image: new Icon({
				src: '/assets/img/icon-dot-trans.png',
				scale: 0.75,
				opacity: 0.5
			})
		});
		return [style];
	}


}
