import {Injectable} from '@angular/core';
import _ from 'lodash';
import {ATTRIBUTION} from 'ol/source/OSM';
import proj4 from 'proj4';
import {get, ProjectionLike, transform, transformExtent} from 'ol/proj';
import {register} from 'ol/proj/proj4';
import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import {Coordinate} from 'ol/coordinate';
import {Extent} from 'ol/extent';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import {MapOptions} from 'ol/PluggableMap';
import View, {FitOptions} from 'ol/View';
import {defaults as interactionDefaults, DefaultsOptions as InteractionOptions} from 'ol/interaction';
import {defaults as controlDefaults, DefaultsOptions as ControlOptions} from 'ol/control';
import ZoomToExtent, {Options as ZoomToExtentOptions} from 'ol/control/ZoomToExtent';
import {StyleLike} from 'ol/style/Style';
import VectorLayer from 'ol/layer/Vector';
import {Options as LayerOptions} from 'ol/layer/BaseVector';
import {TileCacheType} from '../classes/tileCacheType';
import VectorSource from 'ol/source/Vector';
import XYZ from 'ol/source/XYZ';

@Injectable({
	providedIn: 'root'
})
export class MapService {

	PROJ_4326: ProjectionLike = get('EPSG:4326');
	PROJ_3857: ProjectionLike = get('EPSG:3857');
	PROJ_28992: ProjectionLike = null;

	// The Netherlands => [minx, miny, maxx, maxy]
	the_netherlands_bbox: [number,number,number,number] = [2.862080, 50.719120, 8.064106, 53.696754];
	nl_extent: Extent = transformExtent(this.the_netherlands_bbox, this.PROJ_4326, this.PROJ_3857);

	// default tile endpoint
	tiles_url: string = 'https://tilecache.irias.nl/tiles/';
	tiles_url_2x: string = 'https://tilecache.irias.nl/tiles/';

	constructor() {
		proj4.defs(
			'EPSG:28992',
			'+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs');
		register(proj4);

		this.PROJ_28992 = get('EPSG:28992');
	}

	createMap(target: string, type: TileCacheType = TileCacheType.OSM): Map {
		let map_options = {
			target: target,
			view: new View({
				maxZoom: 17,
				extent: this.nl_extent,
				showFullExtent: true,
				smoothExtentConstraint: true
			}),
			interactions: interactionDefaults({
				onFocusOnly: true,
				pinchZoom: true,
				mouseWheelZoom: true,
				doubleClickZoom: true,
				altShiftDragRotate: false
			}),
			controls: controlDefaults({
				zoom: false,
				rotate: false,
				attribution: true,
				attributionOptions: {
					collapsible: false
				}
			}),
			layers: [
				this.getTileCacheLayer(type)
			]
		} as MapOptions;

		let map = new Map(map_options);

		let elem = document.createElement('ion-icon');
		elem.setAttribute('name', 'expand');

		this.updateZoomControl(map, this.nl_extent, elem, true);
		return map;
	}

	private getTileCacheLayer(type: TileCacheType, extension: string = 'png') {
		const url = `${this.tiles_url}${type}/{z}/{x}/{y}.${extension}`;
		const url_2x = `${this.tiles_url_2x}${type}/@2x/{z}/{x}/{y}.${extension}`;

		return this.tileLayer(url, url_2x);
	};

	private tileLayer(url: string, url_2x: string) {
		let ratio = 1;
		let ration_2 = 2;

		// disable 2x for now
		url_2x = url;
		ration_2 = ratio;

		let XYZ_source = new XYZ({
			url: (url_2x && window.devicePixelRatio > 1.5) ? url_2x : url,
			tilePixelRatio: (url_2x && window.devicePixelRatio > 1.5) ? ration_2 : ratio,
			attributions: [
				ATTRIBUTION // https://www.mapbox.com/help/how-attribution-works/#other-mapping-frameworks
			]
		});

		return new TileLayer({source: XYZ_source});
	}

	updateZoomControl(map: Map, extent: Extent, labelOrElement: string|HTMLElement, fit: boolean = false, className: string = 'ol-zoom-extent') {
		map.getControls().forEach(control => {
			if(control instanceof ZoomToExtent) {
				map.removeControl(control);
			}
		});

		let properties = {
			extent: extent,
			label: labelOrElement,
			tipLabel: 'Zoom naar startgebied',
			className: className
		} as ZoomToExtentOptions;

		let zoomControl = new ZoomToExtent(properties);
		map.addControl(zoomControl);

		if(fit) {
			this.fit(map, extent);
		}
	};

	fit(map: Map, geometryOrExtent: SimpleGeometry | Extent, opt_options?: FitOptions): void {
		let options = {
			padding: [50, 50, 50, 50], // [top,right,bottom,left] in pixels
			constrainResolution: true, // Constrain the resolution
			size: map.getSize()
		} as FitOptions;

		options = _.assignIn(options, opt_options);
		map.getView().fit(geometryOrExtent, options);
	};

	getLayer(style: StyleLike, zIndex: number): VectorLayer {
		let layer = this.getLayerSimple();
		layer.setZIndex(zIndex);
		layer.setStyle(style);

		return layer;
	};

	getLayerSimple(): VectorLayer {
		let options = {
			updateWhileAnimating: true,
			updateWhileInteracting: true,
			visible: true,
			source: new VectorSource()
		} as LayerOptions;

		return new VectorLayer(options);
	};



	/* UTIL */

	transformTo4326(c: Coordinate): Coordinate {
		return transform(c, this.PROJ_3857, this.PROJ_4326);
	}

	transformTo3857(c: Coordinate, fromLatLong: boolean = false): Coordinate {
		if(fromLatLong) {
			return transform(c, this.PROJ_4326, this.PROJ_3857);
		}

		return transform([_.toNumber(c[0]), _.toNumber(c[1])], this.PROJ_28992, this.PROJ_3857);
	}

	transformTo28992(c: Coordinate): Coordinate {
		let t = transform(c, this.PROJ_3857, this.PROJ_28992);
		t[0] = Math.round(t[0]);
		t[1] = Math.round(t[1]);
		return t;
	}
}
