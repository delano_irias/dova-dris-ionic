import { TestBed } from '@angular/core/testing';

import { SherpaService } from './sherpa.service';

describe('SherpaService', () => {
  let service: SherpaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SherpaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
