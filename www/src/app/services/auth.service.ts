import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from "rxjs";
// import {auth, modal, msg, util} from "../../services";
// import {PasswordComponent} from "../components/modals/password/password.component";
import _ from "lodash";
import {global} from "../../globals";
// import {PasswordResetComponent} from "../components/modals/password-reset/password-reset.component";
import {AuthParameter, User} from "../classes/auth/User";
import {SherpaError} from '../classes/sherpa/SherpaError';
import {ToastController} from '@ionic/angular';
import {SherpaService} from "./sherpa.service";

@Injectable({
	providedIn: 'root'
})
export class AuthService implements CanActivate {

	private _sessionUser: User; // todo real object

	constructor(private router: Router, private toastCtrl: ToastController, public sherpa: SherpaService) {
	}

	clear() {
		this.removeToken();
		this.setUser({});
		window.location.href = '/';
	}

	setUser(user) {
		this._sessionUser = user;
	}

	whoamiPromise() {
		if (!!this.token()) {

			// Look into why whoami2 is not available on refresh
			// Maybe circular dependency error of auth?

			try {
				return this.sherpa.AuthApi.whoami2(this.token())
					.then((user: any) => {
						this.setUser(user);
						return this._sessionUser;
					})
					.catch((error: SherpaError) => {
						return Promise.reject(error)
					});
			}catch(error: any){
				console.log(error);
			}
		}

		return Promise.reject('Invalid token');
	}

	getUserParameterValue(key : string) {

		if(this._sessionUser.params !== null && this._sessionUser.params !== undefined){
			let f:AuthParameter = _.find(this._sessionUser.params, (up : AuthParameter) => {
				return up.key === key;
			});
			if(f !== null && f !== undefined){
				return f.value;
			}
		}
		return null;
	}

	getUser() {
		return this._sessionUser;
	}

	login(credentials): Promise<any> {
		let app = {
			app_code: environment.appCode
		};

		return this.sherpa.AuthApi.login(credentials.username, credentials.password, app).then(
			(data: any) => {
				this.setUser(data.user);
				this.setToken(data.token);
			}, (e) => {
				console.log('login failed', e);
				return Promise.reject(e);
			}
		);
	}


	logout(): Promise<any> {
		return this.sherpa.AuthApi.logout(this.token()).then(
			result => {
				this.clear();
			}, e => {
			console.log('logout failed', e);
			this.clear();
		})
	}

	token(): string {
		return sessionStorage.getItem('token') || '';
	}

	setToken(token) {
		return sessionStorage.setItem('token', token);
	}

	removeToken() {
		sessionStorage.removeItem('token');
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		let tokenFromUrl = route.queryParams['login'];
		if(tokenFromUrl !== null && tokenFromUrl !== undefined){
			this.setToken(tokenFromUrl);
		}

		return this.whoamiPromise()
			.then((user: User) => {
				global.loginUser = user;
				return true;
			})
			.catch((error) => {
				console.log('error whoami', error);
				this.removeToken();
				this.setUser({});
				this.openLogin(state.url);
				return false;
			});
	}


	public openLogin(requested_url: string) {
		this.router.navigate(['/login'], {queryParams: {returnto: requested_url}});
	}

	async showToast(message: string) {
		const toast = await this.toastCtrl.create({
			message: message + '',
			position: 'top',
			buttons: [
				{
					text: 'Sluit',
					role: 'cancel',
					handler: () => {}
				}
			]
		});
		toast.present();
	}
}
