import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Sherpa, SherpaApi} from '../classes/sherpa/Sherpa';
import {SherpaJson} from '../classes/sherpa/SherpaJson';
import {catchError, map, pluck, retry, share, shareReplay} from 'rxjs/operators';
import {SherpaResponse} from '../classes/sherpa/SherpaResponse';
import {SherpaError} from '../classes/sherpa/SherpaError';
import _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class SherpaService {

	SherpaJson = 'https://basisregistratie-test.ov-data.nl/api/sherpa.json';
	// SherpaJson = 'http://localhost:8040/api/sherpa.json';
	api: SherpaApi = {} as SherpaApi;
	docs: SherpaJson;
	AuthSherpaJson = 'https://auth.ov-data.nl/api/sherpa.json';
	AuthApi: SherpaApi = {} as SherpaApi;
	AuthDocs: SherpaJson;

	private readonly httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json'
		})
	};

	constructor(private http: HttpClient) {
	}

	async buildSherpa(strategy: ErrorMessageStrategy = new ConsoleErrorMessage()) {
		const BaseSherpa = await this.setupSherpa(this.SherpaJson, strategy).toPromise();
		this.api = BaseSherpa.api;
		this.docs = BaseSherpa.json;
		const AuthSherpa = await this.setupSherpa(this.AuthSherpaJson, strategy).toPromise();
		this.AuthApi = AuthSherpa.api;
		this.AuthDocs = AuthSherpa.json;
	}

	/**
	 * Setting up a new Sherpa.
	 * 1. rest-call to sherpa.json
	 * 2. for each function in json.functions create a http.post.toPromise()
	 */
	private setupSherpa(url: string, strategy: ErrorMessageStrategy): Observable<Sherpa> {
		return this.rest<SherpaJson>(url, true)
			.pipe(
				map((result: SherpaJson) => {
					let sherpa = new Sherpa(result);
					const baseUrl = _.trimEnd(sherpa.json.baseurl, '/') + '/';

					_.forEach(sherpa.json.functions, (fn: string) => {
						sherpa.api[fn] = <T>(...args: any[]) => this.sherpaPost(baseUrl + fn, {params: args}, strategy);
					});

					return sherpa as Sherpa;
				})
			);
	}

	sherpaPost<T>(url: string, body: any, strategy: ErrorMessageStrategy): Promise<T> {
		return this.http
			.post<SherpaResponse>(url, JSON.stringify(body), this.httpOptions)
			.toPromise()
			.then((response: any) => {

				// call succeeded, but error in backend
				if (response.error !== null) {
					return Promise.reject(response.error);
				}

				return response.result as T;

			})
			.catch((error: any) => {
				let sherpaError: SherpaError;

				if (_.has(error, ['code', 'message'])) {
					sherpaError = new SherpaError(error.code, error.message);
				} else if (error instanceof Error) {
					sherpaError = new SherpaError('sherpaClientError', error.message);
				} else if (error.status === 404) {
					sherpaError = new SherpaError('sherpaBadFunction', 'function does not exist');
				} else {
					sherpaError = new SherpaError('sherpaHttpError', error.message);
				}

				// show the sherpaError conform the strategy
				strategy.show(sherpaError);

				return Promise.reject(sherpaError);
			});
	}

	rest<T>(url: string, once: boolean = false): Observable<T> {
		let ob = this.http
			.get<T>(url, this.httpOptions)
			.pipe(
				retry(2), // aka 3 attempts
				catchError((error: HttpErrorResponse) => {
					return throwError(error);
				})
			);

		if (once) {
			return ob.pipe(
				shareReplay() // call it just once
			);
		}

		return ob;
	}
}

/* Strategy pattern; How to show sherpa-errors. Some examples below */

export interface ErrorMessageStrategy {
	show(error: SherpaError): void;
}

export class ConsoleErrorMessage implements ErrorMessageStrategy {
	show(error: SherpaError) {
		console.error(error.message);
	}
}

export class AlertErrorMessage implements ErrorMessageStrategy {
	show(error: SherpaError) {
		window.alert(error.message);
	}
}
