import { Component, OnInit } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../classes/auth/User';
import {SherpaError} from '../../classes/sherpa/SherpaError';
import {auth} from '../../app.module';

@Component({
  selector: 'app-item',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  requested_url: string = "/";

  get controls() {
    return this.loginForm.controls;
  }

  constructor(private fb: FormBuilder, public router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    let credentials = {
      username: this.controls.username.value,
      password: this.controls.password.value
    };

    let url = '/';

    auth.login(credentials)
        .then((loginResult) => {

          if(!!this.requested_url && this.requested_url.length) {
            url = this.requested_url;

            this.router.navigate([url]);
          }

          // todo is_temporary_password does not exist in loginResult
          auth.whoamiPromise().then((user: User) => {
            this.router.navigateByUrl(url).then(() => {
              if(user.is_temporary_password) {
                // auth.passwordChange();
              }
              else if(!user.priv_stat_accepted){
                // auth.askPermission();
              }
            });
          });

        })
        .catch((error: SherpaError) => {});
  }


}
