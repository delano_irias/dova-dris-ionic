import { Component, OnInit } from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../classes/auth/User';
import {SherpaError} from '../../classes/sherpa/SherpaError';
import {auth, sherpa} from '../../app.module';
import {of} from 'rxjs';
import {map} from 'rxjs/operators';
import {NavController} from '@ionic/angular';


@Component({
  selector: 'app-item',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
})
export class PasswordComponent {

  public passwordForm: FormGroup;

  public get controls() {
    return this.passwordForm.controls;
  }

  constructor(private _fb: FormBuilder, private router: Router, public navCtrl: NavController) {

    this.passwordForm = this._fb.group({
      password: ['', [<any> Validators.required, <any> Validators.minLength(8)]],
      password_repeat: ['', <any> Validators.required, <any> this.passwordEqualValidator.bind(this)]
    });

  }

  passwordEqualValidator(control: AbstractControl) {
    return of(this.controls.password.value !== control.value).pipe(
        map(result => result ? {equal: true} : null)
    );
  }


  submit() {
    if (this.passwordForm.invalid) {
      return;
    }

    this.save(this.controls.password.value);
  }

  // will be overridden by authService
  save(password: string) {

    sherpa.AuthApi.changePasswordCurrentUser(auth.token(), password)
        .then(() => {

          const message = 'Wachtwoord is succesvol aangepast.';
          auth.showToast(message).then(r => this.navCtrl.pop());

        })
        .catch((error: SherpaError) => {})
        .finally(() => {});


  }

}
