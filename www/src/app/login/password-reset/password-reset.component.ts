import { Component, OnInit } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../classes/auth/User';
import {SherpaError} from '../../classes/sherpa/SherpaError';
import {auth, sherpa} from '../../app.module';
import {NavController} from '@ionic/angular';


@Component({
  selector: 'app-item',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
})
export class PasswordResetComponent implements OnInit {

  public form: FormGroup;

  public get controls() {
    return this.form.controls;
  }

  constructor(private _fb: FormBuilder, public navCtrl: NavController) {
  }

  ngOnInit() {
    this.form = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  submit() {
    if(this.form.invalid) {
      return;
    }

    this.confirm(this.controls.email.value);
  }

  confirm(email: string) {
    //confirm box here....


      sherpa.AuthApi.requestPasswordReset(email, 'dova-ddd-test')
          .then(() => {
            const message = 'Er is een e-mail verstuurd naar: ' + email + ' met de vervolginstructies.';
            auth.showToast(message).then(r => this.navCtrl.pop());
          })
          .catch((error: SherpaError) => {});
    };

}

