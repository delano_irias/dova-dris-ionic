import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import _ from 'lodash';
import {auth, customer, sherpa} from '../app.module';
import {SherpaError} from '../classes/sherpa/SherpaError';
import jsQR from 'jsqr';
import {LoadingController, Platform, ToastController} from '@ionic/angular';
import {ActivatedRoute, ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {Dris} from "../classes/Dris";

@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: ['tab1.page.scss']
})
export class Tab1Page{
	@ViewChild('video', { static: false }) video: ElementRef;
	@ViewChild('canvas', { static: false }) canvas: ElementRef;
	@ViewChild('fileinput', { static: false }) fileinput: ElementRef;


	canvasElement: any;
	videoElement: any;
	canvasContext: any;
	scanActive = false;
	scanResult = null;
	loading: HTMLIonLoadingElement = null;

	public dris_id = '';

	constructor(
		private toastCtrl: ToastController,
		private loadingCtrl: LoadingController,
		private plt: Platform,
		private route: ActivatedRoute
	) {
		const isInStandaloneMode = () =>
			'standalone' in window.navigator && window.navigator['standalone'];
		if (this.plt.is('ios') && isInStandaloneMode()) {
			console.log('I am a an iOS PWA!');
			// E.g. hide the scan functionality!
		}

		this.scanResult = null;

		this.route.queryParams.subscribe(params => {
			if (params.dris_id) {
				this.dris_id = params.dris_id;
			}
			if (params.qrcode){
				this.scanResult = params.qrcode;
			}else{
				this.scanResult = null;
			}
		});
	}

	ngAfterViewInit() {
		this.canvasElement = this.canvas.nativeElement;
		this.canvasContext = this.canvasElement.getContext('2d');
		this.videoElement = this.video.nativeElement;
	}

	// Helper functions
	async showQrToast() {
		const toast = await this.toastCtrl.create({
			message: `Open ${this.scanResult}?`,
			position: 'top',
			buttons: [
				{
					text: 'Open',
					handler: () => {
						window.open(this.scanResult, '_system', 'location=yes');
					}
				}
			]
		});
		toast.present();
	}

	reset() {
		this.scanResult = null;
	}

	stopScan() {
		this.scanActive = false;
		this.videoElement.srcObject.getTracks().forEach(function(track) { track.stop(); });
	}

	async startScan() {
		// Not working on iOS standalone mode!
		const stream = await navigator.mediaDevices.getUserMedia({
			video: { facingMode: 'environment' }
		});

		this.videoElement.srcObject = stream;
		// Required for Safari
		this.videoElement.setAttribute('playsinline', true);

		this.loading = await this.loadingCtrl.create({});
		await this.loading.present();

		this.videoElement.play();
		requestAnimationFrame(this.scan.bind(this));
	}

	async scan() {
		if (this.videoElement.readyState === this.videoElement.HAVE_ENOUGH_DATA) {
			if (this.loading) {
				await this.loading.dismiss();
				this.loading = null;
				this.scanActive = true;
			}

			this.canvasElement.height = this.videoElement.videoHeight;
			this.canvasElement.width = this.videoElement.videoWidth;

			this.canvasContext.drawImage(
				this.videoElement,
				0,
				0,
				this.canvasElement.width,
				this.canvasElement.height
			);
			const imageData = this.canvasContext.getImageData(
				0,
				0,
				this.canvasElement.width,
				this.canvasElement.height
			);
			const code = jsQR(imageData.data, imageData.width, imageData.height, {
				inversionAttempts: 'dontInvert'
			});

			if (code) {
				this.scanActive = false;
				this.scanResult = code.data;
				// this.showQrToast();
			} else {
				if (this.scanActive) {
					requestAnimationFrame(this.scan.bind(this));
				}
			}
		} else {
			requestAnimationFrame(this.scan.bind(this));
		}
	}

	captureImage() {
		this.fileinput.nativeElement.click();
	}

	handleFile(files: FileList) {
		const file = files.item(0);

		var img = new Image();
		img.onload = () => {
			this.canvasContext.drawImage(img, 0, 0, this.canvasElement.width, this.canvasElement.height);
			const imageData = this.canvasContext.getImageData(
				0,
				0,
				this.canvasElement.width,
				this.canvasElement.height
			);
			const code = jsQR(imageData.data, imageData.width, imageData.height, {
				inversionAttempts: 'dontInvert'
			});

			if (code) {
				this.scanResult = code.data;
				// this.showQrToast();
			}
		};
		img.src = URL.createObjectURL(file);
	}

	addCodeToDris(){
		if (!!this.dris_id && !!this.scanResult) {
			console.log(this.dris_id);
			let code = this.scanResult;
			code = code.replace('https://www.drismelding.nl/static/qr?c=', '');

			sherpa.api.updateDRISQRCode(auth.token(), this.dris_id, code)
				.then((result: any) => {
					auth.showToast("De QR-Code is aangepast");
					console.log("added: " + code);
				})
				.catch((error: SherpaError) => {

				})
				.finally(() => {
				});
		}
	}

}
