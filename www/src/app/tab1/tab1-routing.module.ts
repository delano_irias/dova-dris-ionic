import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteDetailComponent } from '../route/route-detail/route-detail.component';
import { Tab1Page } from './tab1.page';
import {AuthService} from '../services/auth.service';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
    canActivate: [AuthService],
  }
  , {
    path: 'detail',
    component: RouteDetailComponent,
    canActivate: [AuthService],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule { }
