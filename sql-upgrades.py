#!/usr/bin/env python
# coding=utf-8

import sys, os, os.path, subprocess, re, json, md5, shutil
import buildlib as bl

destination = 'src/main/resources/irias/app/www'
frontend_destination = 'target/classes/irias/app/www'
srcdir = os.path.split(os.path.dirname(sys.argv[0]))[1] + '/'  # figure out the path-relative directory name where this script resides


def build(dest):
	target = bl.make_target_fn(dest)

	sql = []
	for f in sorted(os.listdir('sql')):
		if not re.search('[0-9]{3}-.*\\.sql', f):
			continue
		versionstr = f.split('-')[0]
		if versionstr == '000':
			version = 0
		else:
			version = int(versionstr, 10)
		sql.append(dict(version=version, filename=f))
	def sql_json():
		for e in sql:
			e['sql'] = open('sql/' + e['filename']).read()
		return json.dumps(sql)
	d = target('../sql.json')
	bl.test(d, ['sql'] + ['sql/' + e['filename'] for e in sql], lambda: bl.write(d, sql_json()))


def usage():
	print >>sys.stderr, 'usage: build.py [clean | install | frontend] ...'
	sys.exit(1)

def main(prog, *args):
	args = list(args)
	if args == []:
		args = ['install']
	for t in args:
		if len(args) > 1:
			print >>sys.stderr, '# %s:' % t

		if t == 'clean':
			bl.remove_tree(frontend_destination)

		elif t == 'sql':
			build(destination)
			bl.remove_tree(frontend_destination)

		elif t == 'frontend':
			build(frontend_destination)

		else:
			usage()

if __name__ == '__main__':
	main(*sys.argv)
