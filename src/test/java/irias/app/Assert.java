package irias.app;

import static org.junit.Assert.assertTrue;

public class Assert {
	public static void assertThrows(Class<?> exp, Testable testable) {
		try {
			testable.run();
			assertTrue(String.format("Expected exception %s to be thrown", exp), false);
		} catch (Exception e) {
			assertTrue(String.format("Expected class %s to be thrown, but %s was thrown instead", exp, e.getClass()), exp.isAssignableFrom(e.getClass()));
		}
	}
}
