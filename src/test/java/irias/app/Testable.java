package irias.app;

@FunctionalInterface
public interface Testable {
	void run() throws Exception;
}
