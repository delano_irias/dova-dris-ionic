package irias.app.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import irias.app.Server;
import irias.app.models.*;
import irias.app.models.SherpaResponse;
import irias.app.models.auth.LoginResult;
import irias.app.models.auth.User;
import nl.irias.common.Log;
import nl.irias.sherpa.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Passhtrough api functions. Will be passed to registered base registration API
 */
@SherpaSection(title = "Baseregistration")
public class Baseregistration {

	static LoginResult login;


	public static <T> T callBaseRegistrationSherpaFunction(TypeReference<T> c, String remoteFunction, List<Object> params) throws SherpaException {
		String remoteApi = Server.config.baseRegistrationApi.url;
		return callSherpaFunction(remoteApi, c, remoteFunction, params);
	}

	public static synchronized LoginResult loginAtBaseRegistration() throws SherpaException {

		boolean doLogin = false;
		if (login == null) {
			doLogin = true;
		} else {
			// check login validality
			ArrayList<Object> params = new ArrayList<>();
			params.add(login.token);
			try {
				Baseregistration.callBaseRegistrationSherpaFunction(new TypeReference<SherpaResponse<Void>>() {
				}, "whoami", params);
			} catch (Exception e) {
				doLogin = true;
				login = null;
				Log.warning("Login has been invalidated, need to login again");
			}
		}

		if (doLogin) {
			String remoteFunction = "login";
			List<Object> params = new ArrayList<>();
			params.add(Server.config.baseRegistrationApi.username);
			params.add(Server.config.baseRegistrationApi.password);
			params.add(null);

			SherpaResponse<LoginResult> apiResult = callSherpaFunction(Server.config.auth.getApi(), new TypeReference<SherpaResponse<LoginResult>>() {
			}, remoteFunction, params);
			login = apiResult.result;
		}

		return login;
	}

	public static void logoutFromBaseRegistration(LoginResult u) throws SherpaException {
		String remoteFunction = "logout";
		List<Object> params = new ArrayList<>();
		params.add(u.token);

		SherpaResponse<Void> apiResult = Baseregistration.callBaseRegistrationSherpaFunction(new TypeReference<SherpaResponse<Void>>() {
		}, remoteFunction, params);
	}

	public static User whoami(String token) throws SherpaException {
		String remoteFunction = "whoami";
		List<Object> params = new ArrayList<>();
		params.add(token);

		SherpaResponse<User> apiResult = Baseregistration.callBaseRegistrationSherpaFunction(new TypeReference<SherpaResponse<User>>() {
		}, remoteFunction, params);
		return apiResult.result;
	}

	public static <T> T callSherpaFunction(String remoteApi, TypeReference<T> c, String remoteFunction, List<Object> params) throws SherpaException {

		remoteApi = FilenameUtils.getPathNoEndSeparator(remoteApi) + "/";

		ObjectMapper om = new ObjectMapper();

		HashMap<String, List<Object>> request = new HashMap<>();
		request.put("params", params);

		HttpPost httpPost = new HttpPost(remoteApi + remoteFunction);
		httpPost.setHeader("Content-type", "application/json");
		try {
			httpPost.setEntity(new StringEntity(om.writeValueAsString(request)));
		} catch (UnsupportedEncodingException e) {
			SherpaServerException enew = new SherpaServerException(e.getMessage());
			enew.setStackTrace(e.getStackTrace());
			throw enew;
		} catch (JsonProcessingException e) {
			SherpaServerException enew = new SherpaServerException(e.getMessage());
			enew.setStackTrace(e.getStackTrace());
			throw enew;
		}

		try (CloseableHttpResponse response = HttpClients.createDefault().execute(httpPost)) {
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				throw new SherpaServerException("HTTP error code: " + response.getStatusLine().getStatusCode());
			}

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


			T sr = mapper.readValue(response.getEntity().getContent(), c);
			SherpaResponse<T> sherpaResponse = (SherpaResponse<T>) sr;

			if (sherpaResponse.error != null) {
				if (sherpaResponse.error.containsKey("code")) {
					if ("sherpaNotFound".equalsIgnoreCase(sherpaResponse.error.get("code"))) {
						throw new SherpaNotFoundException(sherpaResponse.error.get("message"));
					} else if ("sherpaNotAllowed".equalsIgnoreCase(sherpaResponse.error.get("code"))) {
						throw new SherpaPermissionDeniedException(sherpaResponse.error.get("message"));
					} else if ("sherpaBadAuth".equalsIgnoreCase(sherpaResponse.error.get("code"))) {
						throw new SherpaBadAuthException(sherpaResponse.error.get("message"));
					} else {
						throw new SherpaServerException(sherpaResponse.error.get("message"));
					}
				}
			}

			return sr;
		} catch (ClientProtocolException e) {
			SherpaServerException enew = new SherpaServerException(e.getMessage());
			enew.setStackTrace(e.getStackTrace());
			throw enew;
		} catch (IOException e) {
			SherpaServerException enew = new SherpaServerException(e.getMessage());
			enew.setStackTrace(e.getStackTrace());
			throw enew;
		}
	}


}
