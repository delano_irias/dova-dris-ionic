package irias.app.api;

import com.fasterxml.jackson.core.type.TypeReference;
import irias.app.Server;
import irias.app.models.*;
import irias.app.models.auth.LoginResult;
import nl.irias.common.geo.Webmercator2Wgs;
import nl.irias.sherpa.SherpaException;
import nl.irias.sherpa.SherpaFunction;
import nl.irias.sherpa.SherpaSection;
import nl.irias.sherpa.SherpaUserException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SherpaSection(title = "Drisscanner", synopsis = "Drisscanner API")
public class Drisscanner {



	@SherpaFunction(synopsis = "Returns true if api status of dova base registration api is OK")
	public static boolean status() throws Exception {
			return _status();
	}

	public static boolean _status() throws Exception {
		return true;
	}

}
