package irias.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.prometheus.client.Counter;
import io.prometheus.client.Histogram;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;
import irias.app.api.Baseregistration;
import irias.app.api.Drisscanner;
import irias.app.models.SherpaResponse;
import irias.app.models.*;
import irias.app.models.auth.AuthConfig;
import nl.irias.common.Log;
import nl.irias.common.db.Pool;
import nl.irias.sherpa.*;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.MultipartConfigElement;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;


public class Server {
	final static int DB_VERSION = 1;

	static Pool connectionPool;
	public static Config config;

	static class SherpaThrowableFormatter implements ThrowableFormatter {
		@Override
		public String format(Throwable t) {
			if (Server.config.showSherpaErrors) {
				return t.toString();
			}
			return "Er is iets misgegaan met het verzoek. Probeer het later nog eens, of neem contact met ons op.";
		}
	}

	static class ExceptionTransformer implements SherpaExceptionTransformer {
		@Override
		public Exception transform(Exception e) {
			if (e instanceof SQLException) {
				SQLException ee = (SQLException) e;
				String state = null;
				try {
					state = ee.getSQLState();
				} catch (Exception x) {
					// nothing, must not make things worse (and this will happen in our code)
				}

				// see https://www.postgresql.org/docs/9.6/static/errcodes-appendix.html
				if (state != null) {
					switch (state) {
					case "23503": // still references to this object
						return new SherpaUserException("Er zijn nog verwijzigingen naar dit object.");
					case "23505": // values not unique
						return new SherpaUserException("Waarden zijn niet uniek.");
					case "23514": // check violation
						return new SherpaUserException("Ongeldige waarde(s).");
					}
				}
			}
			return e;
		}
	}

	static InputStream getMailLogoStream() throws IOException {
		InputStream r = Server.class.getClassLoader().getResourceAsStream("irias/app/mail/logo.png");
		if (r == null) {
			throw new IOException("File for logo does not exist");
		}
		return r;
	}

	//commentaar

	static InputStream getPdfLogoStream() throws IOException {
		// note: you might want to replace this with a different image
		return getMailLogoStream();
	}

	public static void main(String[] args) {
		// prevent annoying surprises, like String.format("%f") doing different things on different machines.
		Locale.setDefault(new Locale("en", "US"));

		// need to manually exit. the jvm just hangs when main throws an exception...
		try {
			if (args.length == 0) {
				usage("missing subcommand");
			}
			String cmd = args[0];
			args = Arrays.copyOfRange(args, 1, args.length);
			switch (cmd) {
			case "serve":
				serve(args);
				break;
			case "version":
				version(args);
				break;
			default:
				usage("unknown subcommand");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	static void version(String[] args) {
		if (args.length != 0) {
			usage("wrong number of arguments");
		}
		System.out.println(getVersion());
		System.out.println("database version " + DB_VERSION);
	}

	static void usage(String s) {
		System.err.println(s);
		System.err.println("usage: drisscanner serve [--bind localhost] [--port 8040] config.json");
		System.err.println("       drisscanner version");
		System.exit(2);
	}

	static String getVersion() {
		String version = Server.class.getPackage().getImplementationVersion();
		if (version == null) {
			return "dev";
		}
		return version;
	}

	static void parseConfig(String path) throws Exception {
		config = new ObjectMapper().readValue(new File(path), Config.class);
		if (config.baseURL == null) {
			throw new Exception("Missing \"baseURL\" section in config file.");
		}
//		if(config.baseRegistrationApi == null){
//			String msg = "Missing baseRegistrationApi section in config file:\nFor example:\nbaseRegistrationApi: ";
//			BaseregistrationApiConfig example = new BaseregistrationApiConfig();
//			example.url = "https://basisregistratie.irias.nl";
//			example.password = "GEHEIM";
//			example.username = "api";
//			ObjectMapper om = new ObjectMapper();
//			msg += om.writerWithDefaultPrettyPrinter().writeValueAsString(example);
//
//			throw new Exception(msg);
//		}
//		if(config.auth == null){
//
//			String msg = "Missing \"auth\" section in config file:\nFor example:\nauth: ";
//			AuthConfig example = new AuthConfig();
//			example.baseUrl = "https://auth.irias.nl";
//			example.appCode = "dova-ddd-test";
//			ObjectMapper om = new ObjectMapper();
//			msg += om.writerWithDefaultPrettyPrinter().writeValueAsString(example);
//
//			throw new Exception(msg);
//		}
//		if(config.dataDir == null){
//			throw new Exception("Missing \"dataDir\" section in config file. ");
//		}
//
//		if(!new File(config.dataDir).isDirectory()){
//			throw new Exception("Configured \"dataDir\" in config file is not a directory: " + new File(config.dataDir).getAbsolutePath());
//		}

	}




	static void serve(String[] args) throws Exception {
		String bind = "localhost";
		int port = 8040;

		// it seems java standard library doesn't have a way to parse command-line arguments
		int argsIndex = 0;
	Arguments:
		while (argsIndex < args.length && args[argsIndex].startsWith("--")) {
			String opt = args[argsIndex];
			argsIndex += 1;
			switch (opt) {
			case "--":
				break Arguments;
			case "--bind":
				if (argsIndex >= args.length) {
					usage(String.format("flag %s requires parameter", opt));
				}
				bind = args[argsIndex];
				argsIndex += 1;
				break;
			case "--port":
				if (argsIndex >= args.length) {
					usage(String.format("flag %s requires parameter", opt));
				}
				port = Integer.parseInt(args[argsIndex]);
				argsIndex += 1;
				break;
			default:
				usage(String.format("flag %s unrecognized", opt));
			}
		}
		args = Arrays.copyOfRange(args, argsIndex, args.length);
		if (args.length != 1) {
			usage(String.format("need exactly 1 argument, saw %d arguments", args.length));
		}

		parseConfig(args[0]);

		org.eclipse.jetty.server.Server server = new org.eclipse.jetty.server.Server(new InetSocketAddress(bind, port));

		ServletContextHandler handler = new ServletContextHandler();
		ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();

		// map all 404's to base-url (aka /index.html)
		// angular will determine if the path exists
		errorHandler.addErrorPage(404, "/index.html");
		handler.setErrorHandler(errorHandler);

		if (Server.class.getClassLoader().getResource("irias/app/www") == null) {
			throw new Exception("resource directory irias/app/www/ does not exist");
		}
		handler.setResourceBase(Server.class.getClassLoader().getResource("irias/app/www").toString());

		ServletHolder noCacheStaticHandler = new ServletHolder(new DefaultServlet());
		noCacheStaticHandler.setInitParameter("cacheControl", "public, no-cache, max-age=0");



		ServletHolder cacheStaticHandler = new ServletHolder(new DefaultServlet());
		cacheStaticHandler.setInitParameter("cacheControl", String.format("public, max-age=%d", 31*24*3600));

		handler.addServlet(noCacheStaticHandler, "");
		handler.addServlet(noCacheStaticHandler, "/index.html");
		handler.addServlet(noCacheStaticHandler, "/robots.txt");
		handler.addServlet(noCacheStaticHandler, "/assets/*");
		handler.addServlet(noCacheStaticHandler, "/*");

		handler.addServlet(cacheStaticHandler, "/favicon.ico");

		Class<?>[] sections = new Class<?>[] {
			REST.class,
			Drisscanner.class
		};

		ThrowableFormatter formatter = new SherpaThrowableFormatter();
		SherpaCollector collector = new PrometheusSherpaCollector("drisscanner");
		ServletHolder sherpaHandler = new ServletHolder(new SherpaServlet("/drisscanner/", "drisscanner", "drisscanner", Server.getVersion(), sections, formatter, collector, new ExceptionTransformer()));

		SherpaDocHelper.describeData( "Drisscanner", User.class);

		handler.addServlet(sherpaHandler, "/drisscanner/*");

		handler.addServlet(new ServletHolder(new MetricsServlet()), "/metrics");
		DefaultExports.initialize();


		ServletHolder csh = new ServletHolder(new REST());
		MultipartConfigElement m = new MultipartConfigElement(Paths.get(config.dataDir, "tmp").toString(), 100*1024*1024, 100*1024*1024, 1*1024*1024);
		csh.getRegistration().setMultipartConfig(m);
		handler.addServlet(csh, "/img/*");
		handler.addServlet(csh, "/pdf/*");
		handler.addServlet(csh, "/sherpa-api/*");

//		ServletHolder widgetServletHolder = new ServletHolder(new WidgetServlet());
//		handler.addServlet(widgetServletHolder, "/widgets/*");

		server.setHandler(handler);

		Log.info(String.format("drisscanner %s, listening on %s:%d", Server.getVersion(), bind, port));
		server.start();
		server.join();
	}

	private static void _checkBaseRegistrationStatus() {
		try {
			Baseregistration.callBaseRegistrationSherpaFunction(new TypeReference<SherpaResponse<Void>>() {}, "status", new ArrayList<>());
		} catch (SherpaException e) {
			Log.severe("It looks like there is no baseregistration api running on " + Server.config.baseRegistrationApi.url);
			System.exit(1);
		}
	}

	static class PrometheusSherpaCollector implements SherpaCollector {
		Counter requests;
		Counter errors;
		Counter serverErrors;
		Counter protocolErrors;
		Counter badFunction;
		Counter javascript;
		Counter json;
		Histogram requestLatency;
		String api;

		public PrometheusSherpaCollector(String api) {
			this.api = api;
			this.requests = Counter.build().name("sherpa_requests_total").help("Total sherpa requests.").labelNames("api", "function").register();
			this.errors = Counter.build().name("sherpa_errors_total").help("Total sherpa error responses.").labelNames("api", "function").register();
			this.serverErrors = Counter.build().name("sherpa_servererrors_total").help("Total sherpa server error responses.").labelNames("api", "function").register();
			this.requestLatency = Histogram.build().buckets(.01, .05, .1, .2, .5, 1, 2, 4, 8, 16).name("sherpa_requests_duration_seconds").help("Sherpa request duration in seconds.").labelNames("api", "function").register();
			this.protocolErrors = Counter.build().name("sherpa_protocol_errors_total").help("Total sherpa protocol errors.").labelNames("api").register();
			this.badFunction = Counter.build().name("sherpa_bad_function_total").help("Total sherpa bad function calls.").labelNames("api").register();
			this.javascript = Counter.build().name("sherpa_javascript_requests_total").help("Total sherpa.js requests.").labelNames("api").register();
			this.json = Counter.build().name("sherpa_json_requests_total").help("Total sherpa.json requests.").labelNames("api").register();
		}

		@Override
		public void sherpaFunctionCalled(String name, boolean error, boolean serverError, double duration) {
			requests.labels(api, name).inc();
			if (error) {
				errors.labels(api, name).inc();
				if (serverError) {
					serverErrors.labels(api, name).inc();
				}
			}
			requestLatency.labels(api, name).observe(duration);
		}

		@Override
		public void sherpaProtocolError() {
			protocolErrors.labels(api).inc();
		}

		@Override
		public void sherpaBadFunction() {
			badFunction.labels(api).inc();
		}

		@Override
		public void sherpaJavascript() {
			javascript.labels(api).inc();
		}

		@Override
		public void sherpaJSON() {
			json.labels(api).inc();
		}
	}
}
