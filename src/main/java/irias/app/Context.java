package irias.app;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.io.IOException;

import nl.irias.sherpa.*;

import nl.irias.common.db.*;

import irias.app.models.*;


public class Context implements AutoCloseable {
	public SimpleConnection db;
	public User user;

	public Context() throws SQLException {
		this.db = new SimpleConnection(Server.connectionPool);
	}

	public static Context requireAdmin(String token) throws SQLException, SherpaException {
		Context ctx = new Context();
		try {
			_checkTokenCommitting(ctx, token, false);
		} catch(Exception e) {
			ctx.close();
			throw e;
		}
		return ctx;
	}

	public static Context requireToken(String token) throws SQLException, SherpaException {
		Context ctx = new Context();
		try {
			_checkTokenCommitting(ctx, token, true);
		} catch(Exception e) {
			ctx.close();
			throw e;
		}
		return ctx;
	}

	/**
	 * Checks if the given token is valid. A token is valid when it exists in
	 * the session table, and the last activity of a user has been less than 24
	 * hours ago.
	 */
	public static void _checkTokenCommitting(Context ctx, String token, boolean allowRegularUser) throws SQLException, SherpaException {
		String q = ""
				+ " select row_to_json(u.*)"
				+ " from session as s"
				+ " join user_without_auth as u on s.user_id = u.id"
				+ " where 1=1"
				+ "         and u.is_active"
				+ "         and s.last_activity > now() - interval '24 hour'"
				+ "         and s.end_time is null"
				+ "         and s.token = ?";

		if (allowRegularUser) {
			q += " and u.role in ('admin', 'user')";
		} else {
			q += " and u.role in ('admin')";
		}

		ResultSet rs = ctx.db.queryNoneOrSingle(q, token);
		if (rs == null) {
			throw new SherpaBadAuthException("Not allowed");
		}

		try {
			ctx.user = Query.parseJson(User.class, rs.getString(1));
		} catch (IOException e) {
			throw new SherpaServerException("Internal error, please contact us");
		}
		ctx.db.updateSingle("update session set last_activity = now() where user_id = ? and token = ?", ctx.user.id, token);
		ctx.db.commit();
	}

	@Override
	public void close() throws SQLException {
		if (this.db == null) {
			return;
		}
		try {
			this.db.rollback();
		} finally {
			Server.connectionPool.put(this.db.connection);
			this.db = null;
		}
	}

	public boolean isAdmin() {
		return "admin".equals(this.user.role);
	}
}
