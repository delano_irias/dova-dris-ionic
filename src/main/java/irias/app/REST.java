package irias.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.irias.common.Log;
import nl.irias.sherpa.SherpaSection;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;

/**
 * To upload files such as images and to download/fetch/use those files in web applications and apps, this API also has several RESTful endpoints. This sections documents those endpoints. Endpoints that create objects generally require an `Authorization` HTTP header with a value `routemaker "<token>"`, with `<token>` replaced with a valid token as returned by `login`.
 *
 * ## POST /img
 * Upload new image.
 *
 * ### Headers
 * - `Authorization` required.
 *
 * ### Query string parameters
 * Ignored
 *
 * ### Request body
 * The body must be `multipart/form-data` with a field named `file` that also sets a filename. For images, this filename must end in `".png"` or `".jpg"`.
 *
 *
 * ## GET /img/<image_id>/img<commands>.{jpg,png}
 * Retrieve processed version of image. The `commands` is a dash-separated list of commands that modify the original image. When requesting an image, you should typically start with a `cut` of the image as described by the `Image` object as returned by `getImage`. Further commands allow resizing, thumbnailing etc. An example URL:
 *
 *	/img/123/img-r100x100x500x600-t50.png
 *
 * The first character of a command-string indicates the command. The remaining string are parameters, typically values separated by an "x".  Valid commands:
 * - `r`, for "rectangle", after the operation only the specified rectangle remains. Four "x"-separated values (two points) must be specified. For example `r100x100x500x500`. Note that the last two values are coordinates, not a width and height.
 * - `t`, for thumbnail, results in a thumbnail. A single value, the max width and max height of the resulting image must be specified. For example `t50`.
 * - `T`, for thumbnail, like `t`, but requires two parameters, the max width and max height specified separately. For example `T200x100`.
 * - `c`, for cutout. A single value, width and height of the resulting image must be specified. The resulting image is of that size, with aspect ratio maintained. This means part of the left/right or top/bottom of the image may be cut away. For example `c100`.
 * - `C`, for cutout, like `c`, but requires two separate parameters for width and height. For example `c200x100`.
 *
 * The result will have a `Cache-Control` header that allows caching for a long time. Images cannot be replaced on the server. New images result in different image id's and therefor different URLs.
 *
 *
 * ## Get /pdf/<userid>/<filename>.pdf
 *
 * Get an example PDF describing a user.
 * NOTE: In your project, you would want to have a token associated with the user (or some other credential check) that must be in the URL. This does not have an access check, which is bad.
 * You can choose `filename` to be anything you want, so you can get a nice name when saving the file.
 */
@SuppressWarnings("serial")
@SherpaSection(title="REST")
public class REST extends HttpServlet {

	private void CORS(HttpServletResponse r) {
		r.setHeader("Access-Control-Allow-Origin", "*");
		r.setHeader("Access-Control-Allow-Methods", "GET, POST");
		r.setHeader("Access-Control-Allow-Headers", "Content-Type");
	}

	private void respond(HttpServletResponse response, Object resp, int status) throws IOException, UnsupportedEncodingException {
		CORS(response);
		response.setHeader("Cache-Control", "no-store");

		OutputStream out = response.getOutputStream();

		response.setContentType("application/json; charset=utf-8");
		response.setStatus(status);
		new ObjectMapper().writeValue(out, resp);
	}

	@SuppressWarnings("serial")
	static class HandledHttpException extends Exception {
	}

	protected static HandledHttpException httpNotFound(HttpServletResponse response) throws IOException {
		response.setHeader("Cache-Control", "no-store");
		response.sendError(404);
		return new HandledHttpException();
	}

	protected int restParseInt(String s, HttpServletResponse response) throws HandledHttpException, IOException {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException e) {
			throw httpNotFound(response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			_get(request, response);
		} catch (HandledHttpException e) {
			// regular operation, user did something wrong probably
		} catch (ServletException | IOException e) {
			throw e;
		} catch(Exception e) {
			// cannot pass these along unchanged due to doGet() interface requirements.
			Log.exception("error during GET on rest endpoint", e);
			throw new ServletException(e.getMessage());
		}
	}

	protected void _get(HttpServletRequest request, HttpServletResponse response) throws HandledHttpException, ServletException, IOException, Exception {
		String path = request.getServletPath() + request.getPathInfo();
		if (path == null) {
			throw httpNotFound(response);
		}
		path = path.substring(1);

		String [] elems = path.split("/");
		if(elems.length < 2) {
			throw httpNotFound(response);
		}

		if (path.contains("/../")) {
			throw httpNotFound(response);
		}

		if(elems.length == 3 && elems[0].equals("sherpa-api") && elems[1].equals("main")) {
			fetchRemoteSherpa(response, Server.config.baseURL + "/drisscanner/", elems[2]);
		}
		else if(elems.length == 3 && elems[0].equals("sherpa-api") && elems[1].equals("base")) {
			fetchRemoteSherpa(response, Server.config.baseRegistrationApi.url, elems[2]);
		}
		else {
			throw httpNotFound(response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			_post(request, response);
		} catch (HandledHttpException e) {
			// regular operation, user did something wrong probably
		} catch (ServletException | IOException e) {
			throw e;
		} catch(Exception e) {
			// cannot pass these along unchanged due to doPost() interface requirements.
			Log.exception("error during POST on rest endpoint", e);
			throw new ServletException(e.getMessage());
		}
	}

	protected void _post(HttpServletRequest request, HttpServletResponse response) throws HandledHttpException, ServletException, IOException, Exception {
		String path = request.getServletPath() + request.getPathInfo();
		if (path == null) {
			throw httpNotFound(response);
		}
		path = path.substring(1);

		String [] elems = path.split("/");
		if(elems.length < 1) {
			throw httpNotFound(response);
		}

		if (path.contains("/../")) {
			throw httpNotFound(response);
		}

		throw httpNotFound(response);

	}

	private static String getExtension(String name) {
		String[] t = name.split("\\.");
		return t[t.length-1].toLowerCase();
	}


	private void fetchRemoteSherpa(HttpServletResponse extResponse, String remoteApiUrl, String key) throws IOException {
		String remoteApi = FilenameUtils.getPathNoEndSeparator(remoteApiUrl) + "/" + "sherpa.json";
		HttpGet httpGet = new HttpGet(remoteApi);

		try (CloseableHttpResponse response = HttpClients.createDefault().execute(httpGet)) {
			InputStream content = response.getEntity().getContent();
			StringWriter writer = new StringWriter();
			IOUtils.copy(content, writer, Charset.defaultCharset());
			String json = writer.toString();

			respondJS(extResponse, json, key);
		}
	}

	private void respondJS(HttpServletResponse extResponse, String json, String key) throws IOException {
		String js = """
			'use strict';

			(function(undefined) {
				window['%s'] = %s;
			})();
		""";

		js = String.format(js, key, json);
		extResponse.getWriter().println(js);
	}

}
