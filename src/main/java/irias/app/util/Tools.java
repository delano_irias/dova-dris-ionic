package irias.app.util;

import nl.irias.common.Text;

public class Tools {

	public static String appendZeros(String str, int expectedLength){
		if(Text.ensureString(str).length() < expectedLength) {
			int append = expectedLength - str.length();
			for (int i = 0; i < append; i++) {
				str = 0 + str;
			}
			return str;
		}
		else {
			return str;
		}
	}

}
