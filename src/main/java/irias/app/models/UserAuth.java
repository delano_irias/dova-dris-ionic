package irias.app.models;

import java.util.UUID;

// UserAuth contains all sensitive user information that shouldn't be exported through the API
// contains all fields from the "user" table.
// note: when returning a user in the API, return a "User" instead, a "UserAuth" shouldn't leave the app.
public class UserAuth extends User {
	public String salt;
	public String password_hash;
	public UUID password_reset;
}
