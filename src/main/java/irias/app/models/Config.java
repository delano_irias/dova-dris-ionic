package irias.app.models;

import irias.app.models.auth.AuthConfig;
import nl.irias.common.mail.MailConfig;

public class Config {
	public String baseURL;
	public boolean showSherpaErrors;
	public String dataDir;
	public BaseregistrationApiConfig baseRegistrationApi;
	public AuthConfig auth;
	public Boolean onlyAvailableQuays = true;

}
