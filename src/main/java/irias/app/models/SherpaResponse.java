package irias.app.models;

import java.util.HashMap;

/**
 * Created by Harold on 5/2/2017.
 */

public class SherpaResponse<T> {

	public T result;
	public HashMap<String, String> error;

}
