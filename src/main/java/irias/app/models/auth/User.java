package irias.app.models.auth;

import java.util.List;

public class User {
	public Integer user_id;
	public String username;
	public String email;
	public boolean is_root;
	public Boolean is_active;
	public String name;
	public String phone;
	public String address;
	public String notes;

	public List<Clearance> clearance;
}
