package irias.app.models.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Clearance {
	public String division_code;
	public String division_name;
	public String role_code;
}
