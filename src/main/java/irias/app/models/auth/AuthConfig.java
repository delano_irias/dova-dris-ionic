package irias.app.models.auth;

import nl.irias.sherpa.SherpaNotFoundException;

public class AuthConfig {
	public String baseUrl;
	public String appCode;

	public String getApi() throws SherpaNotFoundException {

		if("".equals(baseUrl)) {
			throw new SherpaNotFoundException("baseUrl not found");
		}

		if(baseUrl.endsWith("/")) {
			return baseUrl + "api/";
		}

		return baseUrl + "/api/";
	}
}
