package irias.app.models;

// for view user_without_auth
// note: UserAuth extends this class!
public class User {
	public Integer id;
	public String username;
	public String email;
	public String foreign_code;
	public Boolean is_active;
	public String name;
	public String phone;
	public String address;
	public String notes;
	public String role;
}
